<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('category_id')->unsigned();
            $table->string('menu_name');
            $table->string('menu_price');
            $table->enum('menu_status', ['Ready', 'Empty']);
            $table->timestamps();
        });

        Schema::table('menu', function (Blueprint $table) {
            $table->foreign('category_id')
                ->references('id')
                ->on('category')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop Relation
        Schema::table('menu', function (Blueprint $table) {
            $table->dropForeign('menu_category_id_foreign');
        });

        Schema::dropIfExists('menu');
    }
}
