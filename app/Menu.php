<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id', 'menu_name', 'menu_price', 'menu_status'
    ];

    // Relation with Menu
    public function category(){
        return $this->belongsTo('App\Category', 'category_id');
    }
}
