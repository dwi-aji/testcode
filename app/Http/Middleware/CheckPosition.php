<?php

namespace App\Http\Middleware;

use Closure;

class CheckPosition
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->position == 'Kasir'){
            return $next($request);
        } else {
            return response()->json([
                'message' => "You don't have right access"
            ]);
        }
    }
}
