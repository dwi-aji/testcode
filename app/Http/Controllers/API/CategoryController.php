<?php

namespace App\Http\Controllers\API;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Category::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category;
        $category->category_name = ucwords($request->category_name);

        $category->save();
        return response()->json([
            'message' => 'Successfully created category!'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        try {
            $category = Category::find($category->id);

            return response()->json($category);
        } catch(ModelNotFoundException $e) {
            return response()->json([
                'message' => $e
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        try {
            $category = Category::find($category->id);

            $category->category_name = $request->category_name;
            $category->save();

            return response()->json([
                'message' => 'Successfully updated category!'
            ], 201);
        } catch(ModelNotFoundException $e) {
            return response()->json([
                'message' => $e
            ]);
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        try {
            $category = Category::find($category->id);

            $category->delete();

            return response()->json([
                'message' => 'Successfully deleted category!'
            ], 201);
        } catch(ModelNotFoundException $e) {
            return response()->json([
                'message' => $e
            ]);
        } 
    }
}
