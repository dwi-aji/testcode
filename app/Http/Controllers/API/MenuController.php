<?php

namespace App\Http\Controllers\API;

use App\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenuController extends Controller
{
    public function __construct(){
        $this->middleware('checkposition')->except('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Menu::with('category')->get());        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $menu = new Menu;
        $menu->category_id = $request->category_id;
        $menu->menu_name = $request->menu_name;
        $menu->menu_price = $request->menu_price;
        $menu->menu_status = $request->menu_status;

        $menu->save();
        return response()->json([
            'message' => 'Successfully created menu!'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        try {
            $menu = Menu::find($menu->id)->with('category')->get();

            return response()->json($menu);
        } catch(ModelNotFoundException $e) {
            return response()->json([
                'message' => $e
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        try {
            $menu = Menu::findOrFail($menu->id);
            $menu->category_id = $request->category_id;
            $menu->menu_name = $request->menu_name;
            $menu->menu_price = $request->menu_price;
            $menu->menu_status = $request->menu_status;

            $menu->save();
            return response()->json([
                'message' => 'Successfully updated menu!'
            ], 201);
        } catch(ModelNotFoundException $e) {
            return response()->json([
                'message' => $e
            ]);
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        try {
            $menu = Menu::find($menu->id);

            $menu->delete();

            return response()->json([
                'message' => 'Successfully deleted menu!'
            ], 201);
        } catch(ModelNotFoundException $e) {
            return response()->json([
                'message' => $e
            ]);
        }
    }
}
