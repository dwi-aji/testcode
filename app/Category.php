<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "category";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_name'
    ];

    // Relation with Menu
    public function menu(){
        return $this->hasMany('App\Menu', 'category_id');
    }
}
